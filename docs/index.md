# H5dict

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-green.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html) [![Release](https://img.shields.io/badge/dynamic/json?color=blueviolet&label=Release&query=%24[0].name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Fnanogennari%252Fh5dict%2Frepository%2Ftags&style=flat-square)](https://gitlab.com/nanogennari/h5dict/)


An interface to read/write HDF5 files as if they where dictionaries.

## Installation

h5dict can be installed with pip:

    python -m pip install h5dict

### Manual installation

Clone the repository and run setup.py:

    git clone https://gitlab.com/nanogennari/h5dict.git
    cd h5dict
    python setup.py install

## Usage

Usage example:

    import h5dict
    import numpy as np

    hdf5 = h5dict.File("file.hdf5", "a")
    hdf5["test_1"] = np.arange(100)
    hdf5["test_2"] = {
        "names": ["Alice", "Bob"],
        "ages": [30, 25]
    }

    for key in hdf5["test_2"]["names"].keys():
        print(hdf5["test_2"]["names"][key])

    data = hdf5["test_2"].to_dict()
    hdf5.tree_view(print_types=True)

    hdf5.close()

### Compression

Parameters for h5py compression filters can be passed as well:

    filter = {"compression": "lzf"}
    hdf5["array", filter] = np.arange(10000)

See [h5py's documentation](https://docs.h5py.org/en/stable/high/dataset.html#filter-pipeline) for available filters.

### Type conversion

One of the functionalities of H5dict is the ability to convert data that normally can't be stored directly on HDF5 files using h5py, there is four filters implemented on h5dict:

* NextworkX graphs (Graph and DiGraph)
* lists (including lists of mixed types)
* tuples
* None type
* Numpy random number generator
* datetime

Given the right filter h5dict can store any type of data, filters consists of three parts:

* The type object
* A function to convert the data to a format that can be stored on the HDF5 (dicts, arrays, strings, ints, floats or a mix of them)
* A function to convert the data from the format that can be stored on the HDF5

We can see those tree in the NetworkX filter:

    import networkx as nx
    nx_type = nx.classes.graph.Graph

    def nx_to(G):
        string = ""
        for line in nx.generate_graphml(G):
            string += line + "\n"
        return string

    def nx_from(string):
        return nx.parse_graphml(string)

    filters = [(nx_type, nx_to, nx_from)]

Those tree elements are put in a tuple, one or more additional filters can be passed on the `h5dict.File()` function as a list by the `conversions_filters` argument.

### Fallback mode

The way h5dict will handle incompatible types without conversions can be set with the parameter `fallback_mode`.

The default behavior is `representation` where h5dict tries to store the representation of incompatible types and prints an warning when this happens.

The alternative behavior is to set `fallback_mode` to `representation` when instancing an h5dict object with the [`File`](../reference/#h5dict.h5dict.File) function, in this mode h5dict will store a pickled version of the incompatible object, **without printing warnings**.

!!! warning
    Pickled data can be dangerous is extremely recommended to only load picked data from trusted sources.

!!! warning
    Due to limitations of hdf5 files the pickle serialization must be done with the old and inefficient [protocol 0](https://docs.python.org/3/library/pickle.html#data-stream-format), is not recommended to store large objetcs.
