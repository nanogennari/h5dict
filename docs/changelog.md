# Changelog

## v0.2.3
* Added `fallback_mode` to configure how h5dict stores incompatible formats;
* Fixed `conversions_filters` not been passed to internal class.

## v0.2.2
* Bug fixes with h5py>=3.0.0;
* Dropped support for older h5py versions;
* Updated requirements to require h5py>=3.0.0.

## v0.2.1
* Fixed string reading on h5py 2.x or older.

## v0.2.0
* Using dataset attributes to store type information;
* Fixed string reading for new h5py versions;
* New conversions for numpy random number generator and datetime.

## v0.1.4
* Implemented new tree_view() method to print hdf5 structure.

## v0.1.3
2021-01-08

* Implemented container type methods:
    * \__contains__();
    * \__delitem__();
    * \__enter__();
    * \__exit__();
    * \__iter__();
    * \__len__();
* Better handling of keys that already exists.

## v0.1.2
2020-11-28

* New conversion filters to:
    * NetworkX directional Graph
    * None type
    * datetime

## v0.1.1
2020-09-14

* Added conversion filters;
* Added to_dict method;
* Better handling of multiple group levels on HDF5.

## v0.1.0
2020-09-13

* Initial release.